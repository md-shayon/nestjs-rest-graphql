/* eslint-disable import/no-unresolved */
import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true, // Get read of the property that is not in the body field
      transform: true,
      transformOptions: {
        enableImplicitConversion: true,
      },
    }),
  ); // Pipes are for validation
  await app.listen(3000); // can change port here
}
bootstrap();
