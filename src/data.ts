/* eslint-disable no-shadow */
/* eslint-disable import/prefer-default-export */
import {v4 as uuidv4} from 'uuid';
export enum ReportType {
  INCOME = 'income',
  EXPENSE = 'expense',
}

interface Data {
  [x: string]: any;
  report: {
    id: string;
    source: string;
    amount: number;
    created_at: Date;
    updated_at: Date;
    type: ReportType;
  }[];
}

export const data: Data = {
  report: [
    {
      id: uuidv4(),
      source: 'freelancing',
      amount: 120,
      created_at: new Date(),
      updated_at: new Date(),
      type: ReportType.INCOME,
    },
    {
      id: uuidv4(),
      source: 'business',
      amount: 320,
      created_at: new Date(),
      updated_at: new Date(),
      type: ReportType.INCOME,
    },
    {
      id: uuidv4(),
      source: 'website',
      amount: 420,
      created_at: new Date(),
      updated_at: new Date(),
      type: ReportType.EXPENSE,
    }
  ],
};

// data.report.push({
//   id: 'uuid',
//   source: 'string',
//   amount: 120,
//   created_at: new Date(),
//   updated_at: new Date(),
//   type: ReportType.INCOME,
// });
