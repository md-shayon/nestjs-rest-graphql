/* eslint-disable import/no-unresolved */
/* eslint-disable import/prefer-default-export */
/* eslint-disable no-useless-constructor */
import { Controller, Get } from '@nestjs/common';
import { SummaryService } from './summary.service';

@Controller('/api/summary')
export class SummaryController {
  constructor(private readonly summaryService: SummaryService) {}

  @Get()
  getSummary() {
    return this.summaryService.calculateSummary( );
  }
}
