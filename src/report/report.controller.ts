/* eslint-disable import/extensions */
/* eslint-disable import/no-unresolved */
/* eslint-disable consistent-return */
/* eslint-disable import/prefer-default-export */
import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Param,
  Body,
  HttpCode,
  ParseIntPipe,
  ParseUUIDPipe,
  ParseEnumPipe,
} from '@nestjs/common'; // Decorators
// Every single entity inside of nest js is class
import { data, ReportType } from 'src/data';
import {
  CreateReportDto,
  ReportResponseDto,
  UpdateReportDto,
} from 'src/dtos/report.dto';
import { ReportService } from './report.service';

// Should have the ability to create endpoint using @Controller()
@Controller('/api/report/:type')
export class ReportController {
  // eslint-disable-next-line prettier/prettier, no-useless-constructor
    constructor(private readonly reportService: ReportService) { }

  @Get() // UpdateReportDto is response model
  getAllReports(@Param('type') type: string): ReportResponseDto[] {
    const reportType =
      type === 'income' ? ReportType.INCOME : ReportType.EXPENSE;
    // console.log(type);
    return this.reportService.getAllReports(reportType);
  }

  @Get('/:id')
  getReportById(
    @Param('type') type: string,
    @Param('id', ParseUUIDPipe) id: string,
  ): ReportResponseDto {
    // console.log(id, typeof id);

    const reportType =
      type === 'income' ? ReportType.INCOME : ReportType.EXPENSE;
    return this.reportService.getReportById(reportType, id);
  }

  @Post('/create')
  createReport(
    @Body() { amount, source }: CreateReportDto,
    @Param('type', new ParseEnumPipe(ReportType)) type: string,
  ): ReportResponseDto {
    const reportType =
      type === 'income' ? ReportType.INCOME : ReportType.EXPENSE;
    return this.reportService.createReport(reportType, { amount, source });
  }

  @Put('/update/:id')
  updateReport(
    @Body() body: UpdateReportDto,
    @Param('type') type: string,
    @Param('id', ParseUUIDPipe) id: string,
  ): ReportResponseDto {
    const reportType =
      type === 'income' ? ReportType.INCOME : ReportType.EXPENSE;
    return this.reportService.updateReport(reportType, id, body);
  }

  @HttpCode(204)
  @Delete('/delete/:id')
  deleteReport(@Param('id', ParseUUIDPipe) id: string) {
    return this.reportService.deleteReport(id);
  }
}
