/* eslint-disable import/no-unresolved */
/* eslint-disable import/extensions */
/* eslint-disable import/prefer-default-export */
import { Injectable } from '@nestjs/common';
import { v4 as uuidv4 } from 'uuid';
import { ReportType, data } from 'src/data';
import { ReportResponseDto } from 'src/dtos/report.dto';

interface Report {
  amount: number;
  source: string;
}

interface UpdateReport {
  amount?: number; // Making it option using ?
  source?: string; // Making it option using ?
}

// All business logic should be here
@Injectable()
export class ReportService {
  getAllReports(type: ReportType): ReportResponseDto[] {
    return data.report
      .filter((report) => report.type === type)
      .map((r) => new ReportResponseDto(r));
  }

  getReportById(type: ReportType, id: string) {
    const report = data.report
      .filter((rpt) => rpt.type === type)
      .find((r) => r.id === id);

    if (!report) return;

    // eslint-disable-next-line consistent-return
    return new ReportResponseDto(report);
  }

  createReport(type: ReportType, { amount, source }: Report) {
    const newReport = {
      id: uuidv4(),
      source,
      amount,
      created_at: new Date(),
      updated_at: new Date(),
      type,
    };
    data.report.push(newReport);
    return new ReportResponseDto(newReport);
  }

  updateReport(type: ReportType, id: string, body: UpdateReport) {
    const reportToUpdate = data.report
      .filter((rpt) => rpt.type === type)
      .find((r) => r.id === id);

    // console.log({reportToUpdate, body});

    if (!reportToUpdate) return;
    const reportIndex = data.report.findIndex(
      (rir) => rir.id === reportToUpdate.id,
    );

    data.report[reportIndex] = {
      ...data.report[reportIndex],
      ...body,
      updated_at: new Date(),
    };
    // eslint-disable-next-line consistent-return
    return new ReportResponseDto(data.report[reportIndex]);
  }

  deleteReport(id: string) {
    const reportIndex = data.report.findIndex((rir) => rir.id === id);
    if (reportIndex === -1) return;
    data.report.splice(reportIndex, 1);
  }
}
