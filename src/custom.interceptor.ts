/* eslint-disable import/prefer-default-export */
import { CallHandler, ExecutionContext, NestInterceptor } from '@nestjs/common';
import { Observable, map } from 'rxjs';

export class CustomInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, handler: CallHandler<any>) {
    console.log('Intercepting request');

    console.log(context);

    return handler.handle().pipe(
      map((data) => {
        console.log('Intercepting response');
        console.log(data);

        const response = {
          ...data,
          createdAt: data.created_at,
        };
        delete response.created_at;
        delete response.updated_at;

        return response;
      }),
    );
  }
}
