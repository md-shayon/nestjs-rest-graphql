/* eslint-disable import/prefer-default-export */
import { Controller } from '@nestjs/common'; // Decorators

// Should have the ability to create endpoint using @Controller()
@Controller('/api')
export class AppController {
  // pass
}
